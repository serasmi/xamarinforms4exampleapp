﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace ExampleApp.Shared.Models
{
    [ExcludeFromCodeCoverage]
    public class User
    {
        public string Code { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
        public string Password { get; set; }
        public DateTime Token_Expire_Date { get; set; }
    }
}
