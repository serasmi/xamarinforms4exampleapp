﻿using ExampleApp.Shared.Services.Mocks;
using ExampleApp.Shared.Services.Wrappers.Connectivity;
using ExampleApp.Shared.Services.Wrappers.Credential;
using ExampleApp.Shared.Services.Wrappers.ImgButtonsVisibility;
using ExampleApp.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ExampleApp.Shared.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyItemsPage : ContentPage
    {
        private MyItemsPageViewModel myViewModel;
        public MyItemsPageViewModel MyViewModel
        {
            get => myViewModel;
            set => BindingContext = myViewModel = value;
        }

        public MyItemsPage()
        {
            InitializeComponent();

            MyViewModel = new MyItemsPageViewModel(
                new MyItemsServiceImp(),
                new ButtonsVisibilityImp(
                    MyItemsLayout,
                    new Dictionary<string, Button>
                    {
                        {"edit", this.Edit },
                        {"remove", this.Remove }
                    }),
                new ConnectivityWrapper(),
                new UserSettingsWrapper());

            MyViewModel.LoadCommand.Execute(true);
        }
    }
}