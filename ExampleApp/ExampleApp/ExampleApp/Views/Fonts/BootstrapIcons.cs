﻿namespace ExampleApp.Shared.Views.Fonts
{
    static class BootstrapIcons
    {
        public const string Space = "\u0020";
        public const string Asterisk = "\u002a";
        public const string Plus = "\u002b";
        public const string Uni00A0 = "\u00a0";
        public const string Yen = "\u00a5";
        public const string Uni2000 = "\u2000";
        public const string Uni2001 = "\u2001";
        public const string Uni2002 = "\u2002";
        public const string Uni2003 = "\u2003";
        public const string Uni2004 = "\u2004";
        public const string Uni2005 = "\u2005";
        public const string Uni2006 = "\u2006";
        public const string Uni2007 = "\u2007";
        public const string Uni2008 = "\u2008";
        public const string Uni2009 = "\u2009";
        public const string Uni200A = "\u200a";
        public const string Uni202F = "\u202f";
        public const string Uni205F = "\u205f";
        public const string Euro = "\u20ac";
        public const string Uni20BD = "\u20bd";
        public const string Minus = "\u2212";
        public const string Uni231B = "\u231b";
        public const string Uni25FC = "\u25fc";
        public const string Uni2601 = "\u2601";
        public const string Uni26FA = "\u26fa";
        public const string Uni2709 = "\u2709";
        public const string Uni270F = "\u270f";
        public const string UniE001 = "\ue001";
        public const string UniE002 = "\ue002";
        public const string UniE003 = "\ue003";
        public const string UniE005 = "\ue005";
        public const string UniE006 = "\ue006";
        public const string UniE007 = "\ue007";
        public const string UniE008 = "\ue008";
        public const string UniE009 = "\ue009";
        public const string UniE010 = "\ue010";
        public const string UniE011 = "\ue011";
        public const string UniE012 = "\ue012";
        public const string UniE013 = "\ue013";
        public const string UniE014 = "\ue014";
        public const string UniE015 = "\ue015";
        public const string UniE016 = "\ue016";
        public const string UniE017 = "\ue017";
        public const string UniE018 = "\ue018";
        public const string UniE019 = "\ue019";
        public const string UniE020 = "\ue020";
        public const string UniE021 = "\ue021";
        public const string UniE022 = "\ue022";
        public const string UniE023 = "\ue023";
        public const string UniE024 = "\ue024";
        public const string UniE025 = "\ue025";
        public const string UniE026 = "\ue026";
        public const string UniE027 = "\ue027";
        public const string UniE028 = "\ue028";
        public const string UniE029 = "\ue029";
        public const string UniE030 = "\ue030";
        public const string UniE031 = "\ue031";
        public const string UniE032 = "\ue032";
        public const string UniE033 = "\ue033";
        public const string UniE034 = "\ue034";
        public const string UniE035 = "\ue035";
        public const string UniE036 = "\ue036";
        public const string UniE037 = "\ue037";
        public const string UniE038 = "\ue038";
        public const string UniE039 = "\ue039";
        public const string UniE040 = "\ue040";
        public const string UniE041 = "\ue041";
        public const string UniE042 = "\ue042";
        public const string UniE043 = "\ue043";
        public const string UniE044 = "\ue044";
        public const string UniE045 = "\ue045";
        public const string UniE046 = "\ue046";
        public const string UniE047 = "\ue047";
        public const string UniE048 = "\ue048";
        public const string UniE049 = "\ue049";
        public const string UniE050 = "\ue050";
        public const string UniE051 = "\ue051";
        public const string UniE052 = "\ue052";
        public const string UniE053 = "\ue053";
        public const string UniE054 = "\ue054";
        public const string UniE055 = "\ue055";
        public const string UniE056 = "\ue056";
        public const string UniE057 = "\ue057";
        public const string UniE058 = "\ue058";
        public const string UniE059 = "\ue059";
        public const string UniE060 = "\ue060";
        public const string UniE062 = "\ue062";
        public const string UniE063 = "\ue063";
        public const string UniE064 = "\ue064";
        public const string UniE065 = "\ue065";
        public const string UniE066 = "\ue066";
        public const string UniE067 = "\ue067";
        public const string UniE068 = "\ue068";
        public const string UniE069 = "\ue069";
        public const string UniE070 = "\ue070";
        public const string UniE071 = "\ue071";
        public const string UniE072 = "\ue072";
        public const string UniE073 = "\ue073";
        public const string UniE074 = "\ue074";
        public const string UniE075 = "\ue075";
        public const string UniE076 = "\ue076";
        public const string UniE077 = "\ue077";
        public const string UniE078 = "\ue078";
        public const string UniE079 = "\ue079";
        public const string UniE080 = "\ue080";
        public const string UniE081 = "\ue081";
        public const string UniE082 = "\ue082";
        public const string UniE083 = "\ue083";
        public const string UniE084 = "\ue084";
        public const string UniE085 = "\ue085";
        public const string UniE086 = "\ue086";
        public const string UniE087 = "\ue087";
        public const string UniE088 = "\ue088";
        public const string UniE089 = "\ue089";
        public const string UniE090 = "\ue090";
        public const string UniE091 = "\ue091";
        public const string UniE092 = "\ue092";
        public const string UniE093 = "\ue093";
        public const string UniE094 = "\ue094";
        public const string UniE095 = "\ue095";
        public const string UniE096 = "\ue096";
        public const string UniE097 = "\ue097";
        public const string UniE101 = "\ue101";
        public const string UniE102 = "\ue102";
        public const string UniE103 = "\ue103";
        public const string UniE104 = "\ue104";
        public const string UniE105 = "\ue105";
        public const string UniE106 = "\ue106";
        public const string UniE107 = "\ue107";
        public const string UniE108 = "\ue108";
        public const string UniE109 = "\ue109";
        public const string UniE110 = "\ue110";
        public const string UniE111 = "\ue111";
        public const string UniE112 = "\ue112";
        public const string UniE113 = "\ue113";
        public const string UniE114 = "\ue114";
        public const string UniE115 = "\ue115";
        public const string UniE116 = "\ue116";
        public const string UniE117 = "\ue117";
        public const string UniE118 = "\ue118";
        public const string UniE119 = "\ue119";
        public const string UniE120 = "\ue120";
        public const string UniE121 = "\ue121";
        public const string UniE122 = "\ue122";
        public const string UniE123 = "\ue123";
        public const string UniE124 = "\ue124";
        public const string UniE125 = "\ue125";
        public const string UniE126 = "\ue126";
        public const string UniE127 = "\ue127";
        public const string UniE128 = "\ue128";
        public const string UniE129 = "\ue129";
        public const string UniE130 = "\ue130";
        public const string UniE131 = "\ue131";
        public const string UniE132 = "\ue132";
        public const string UniE133 = "\ue133";
        public const string UniE134 = "\ue134";
        public const string UniE135 = "\ue135";
        public const string UniE136 = "\ue136";
        public const string UniE137 = "\ue137";
        public const string UniE138 = "\ue138";
        public const string UniE139 = "\ue139";
        public const string UniE140 = "\ue140";
        public const string UniE141 = "\ue141";
        public const string UniE142 = "\ue142";
        public const string UniE143 = "\ue143";
        public const string UniE144 = "\ue144";
        public const string UniE145 = "\ue145";
        public const string UniE146 = "\ue146";
        public const string UniE148 = "\ue148";
        public const string UniE149 = "\ue149";
        public const string UniE150 = "\ue150";
        public const string UniE151 = "\ue151";
        public const string UniE152 = "\ue152";
        public const string UniE153 = "\ue153";
        public const string UniE154 = "\ue154";
        public const string UniE155 = "\ue155";
        public const string UniE156 = "\ue156";
        public const string UniE157 = "\ue157";
        public const string UniE158 = "\ue158";
        public const string UniE159 = "\ue159";
        public const string UniE160 = "\ue160";
        public const string UniE161 = "\ue161";
        public const string UniE162 = "\ue162";
        public const string UniE163 = "\ue163";
        public const string UniE164 = "\ue164";
        public const string UniE165 = "\ue165";
        public const string UniE166 = "\ue166";
        public const string UniE167 = "\ue167";
        public const string UniE168 = "\ue168";
        public const string UniE169 = "\ue169";
        public const string UniE170 = "\ue170";
        public const string UniE171 = "\ue171";
        public const string UniE172 = "\ue172";
        public const string UniE173 = "\ue173";
        public const string UniE174 = "\ue174";
        public const string UniE175 = "\ue175";
        public const string UniE176 = "\ue176";
        public const string UniE177 = "\ue177";
        public const string UniE178 = "\ue178";
        public const string UniE179 = "\ue179";
        public const string UniE180 = "\ue180";
        public const string UniE181 = "\ue181";
        public const string UniE182 = "\ue182";
        public const string UniE183 = "\ue183";
        public const string UniE184 = "\ue184";
        public const string UniE185 = "\ue185";
        public const string UniE186 = "\ue186";
        public const string UniE187 = "\ue187";
        public const string UniE188 = "\ue188";
        public const string UniE189 = "\ue189";
        public const string UniE190 = "\ue190";
        public const string UniE191 = "\ue191";
        public const string UniE192 = "\ue192";
        public const string UniE193 = "\ue193";
        public const string UniE194 = "\ue194";
        public const string UniE195 = "\ue195";
        public const string UniE197 = "\ue197";
        public const string UniE198 = "\ue198";
        public const string UniE199 = "\ue199";
        public const string UniE200 = "\ue200";
        public const string UniE201 = "\ue201";
        public const string UniE202 = "\ue202";
        public const string UniE203 = "\ue203";
        public const string UniE204 = "\ue204";
        public const string UniE205 = "\ue205";
        public const string UniE206 = "\ue206";
        public const string UniE209 = "\ue209";
        public const string UniE210 = "\ue210";
        public const string UniE211 = "\ue211";
        public const string UniE212 = "\ue212";
        public const string UniE213 = "\ue213";
        public const string UniE214 = "\ue214";
        public const string UniE215 = "\ue215";
        public const string UniE216 = "\ue216";
        public const string UniE218 = "\ue218";
        public const string UniE219 = "\ue219";
        public const string UniE221 = "\ue221";
        public const string UniE223 = "\ue223";
        public const string UniE224 = "\ue224";
        public const string UniE225 = "\ue225";
        public const string UniE226 = "\ue226";
        public const string UniE227 = "\ue227";
        public const string UniE230 = "\ue230";
        public const string UniE231 = "\ue231";
        public const string UniE232 = "\ue232";
        public const string UniE233 = "\ue233";
        public const string UniE234 = "\ue234";
        public const string UniE235 = "\ue235";
        public const string UniE236 = "\ue236";
        public const string UniE237 = "\ue237";
        public const string UniE238 = "\ue238";
        public const string UniE239 = "\ue239";
        public const string UniE240 = "\ue240";
        public const string UniE241 = "\ue241";
        public const string UniE242 = "\ue242";
        public const string UniE243 = "\ue243";
        public const string UniE244 = "\ue244";
        public const string UniE245 = "\ue245";
        public const string UniE246 = "\ue246";
        public const string UniE247 = "\ue247";
        public const string UniE248 = "\ue248";
        public const string UniE249 = "\ue249";
        public const string UniE250 = "\ue250";
        public const string UniE251 = "\ue251";
        public const string UniE252 = "\ue252";
        public const string UniE253 = "\ue253";
        public const string UniE254 = "\ue254";
        public const string UniE255 = "\ue255";
        public const string UniE256 = "\ue256";
        public const string UniE257 = "\ue257";
        public const string UniE258 = "\ue258";
        public const string UniE259 = "\ue259";
        public const string UniE260 = "\ue260";
        public const string UniF8FF = "\uf8ff";
        public const string U1F511 = "\u0001f511";
        public const string U1F6AA = "\u0001f6aa";
    }
}
