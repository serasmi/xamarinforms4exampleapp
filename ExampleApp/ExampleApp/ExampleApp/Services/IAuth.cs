﻿using System.Threading.Tasks;

namespace ExampleApp.Shared.Services
{
    public interface IAuth
    {
        Task<ExampleApp.Shared.Models.User> Authenticate(Models.User user);
    }
}
