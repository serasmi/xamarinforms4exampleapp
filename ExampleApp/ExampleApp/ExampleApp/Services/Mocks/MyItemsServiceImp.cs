﻿using ExampleApp.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleApp.Shared.Services.Mocks
{
    public class MyItemsServiceImp : IMyItemsService
    {
        private List<Item> items { get; set; } = new List<Item>();

        public MyItemsServiceImp()
        {
            var firstItem = new Item
            {
                Id = Guid.NewGuid().ToString(),
                Name = "This is first item",
                Description = "First test item added to list"
            };

            var secondItem = new Item
            {
                Id = Guid.NewGuid().ToString(),
                Name = "This is second item",
                Description = "Second test item added to list"
            };

            var thirdItem = new Item
            {
                Id = Guid.NewGuid().ToString(),
                Name = "This is third item",
                Description = "Third test item added to list"
            };

            items.Add(firstItem);
            items.Add(secondItem);
            items.Add(thirdItem);
        }

        public Task<bool> AddItem(Item myItem)
        {
            items.Add(myItem);

            return Task.FromResult(true);
        }

        public Task<List<Item>> GetItems(uint page, uint itemsPerPage, string name)
        {
            var list = new List<Item>();

            if(string.IsNullOrEmpty(name))
                list = items.Skip((int)(page * itemsPerPage)).Take((int)itemsPerPage).ToList();
            else
                list = items.Where(s => s.Name.Contains(name)).Skip((int)(page * itemsPerPage)).Take((int)itemsPerPage).ToList();

            return Task.FromResult(list);
        }

        public async Task RemoveItem(string id)
        {
            items.Remove(items.Where(s => string.Equals(s.Id, id)).First());
        }

        public async Task UpdateItem(Item updatedItem)
        {
            var existingItem = items.Where(s => string.Equals(s.Id, updatedItem.Id)).First();

            existingItem = updatedItem;
        }
    }
}
