﻿using ExampleApp.Shared.Models;
using System;
using System.Threading.Tasks;

namespace ExampleApp.Shared.Services.Mocks
{
    public class AuthImp : IAuth
    {
        public Task<ExampleApp.Shared.Models.User> Authenticate(User user)
        {
            var u = new User
            {
                Code = "t",
                Password = "t",
                Token = "t",
                Token_Expire_Date = DateTime.Now,
                Username = "t"
            };

            return Task.FromResult(u);
        }
    }
}
