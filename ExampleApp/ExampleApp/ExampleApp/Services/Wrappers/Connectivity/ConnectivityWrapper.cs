﻿using System;
using System.Diagnostics.CodeAnalysis;
using Xamarin.Essentials;

namespace ExampleApp.Shared.Services.Wrappers.Connectivity
{
    [ExcludeFromCodeCoverage]
    public class ConnectivityWrapper : IConnectivity
    {
        public NetworkAccess GetNetworkState()
        {
            return Xamarin.Essentials.Connectivity.NetworkAccess;
        }

        public void SetEventHandlerToConnectivityChanged(EventHandler<ConnectivityChangedEventArgs> eventHandler)
        {
            Xamarin.Essentials.Connectivity.ConnectivityChanged += eventHandler;
        }
    }
}
