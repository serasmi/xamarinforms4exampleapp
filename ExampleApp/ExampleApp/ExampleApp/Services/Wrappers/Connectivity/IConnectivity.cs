﻿using System;
using Xamarin.Essentials;

namespace ExampleApp.Shared.Services.Wrappers.Connectivity
{
    public interface IConnectivity
    {
        void SetEventHandlerToConnectivityChanged(EventHandler<ConnectivityChangedEventArgs> eventHandler);
        NetworkAccess GetNetworkState();
    }
}
