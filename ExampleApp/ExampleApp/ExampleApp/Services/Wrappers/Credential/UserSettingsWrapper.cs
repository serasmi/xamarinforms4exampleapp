﻿using ExampleApp.Shared.Models;
using System.Diagnostics.CodeAnalysis;

namespace ExampleApp.Shared.Services.Wrappers.Credential
{
    [ExcludeFromCodeCoverage]
    public class UserSettingsWrapper : IUserSettings
    {
        public void ClearAllData() => UserSettings.ClearAllData();

        public string GetPassword() => UserSettings.Password;

        public string GetToken() => UserSettings.Token;

        public string GetTokenExpireDate() => UserSettings.TokenExpireDate;

        public User GetUser()
        {
            return new User
            {
                Code = UserSettings.UserCode,
                Username = UserSettings.UserName,
                Password = UserSettings.Password
            };
        }

        public string GetUserCode() => UserSettings.UserCode;

        public string GetUserName() => UserSettings.UserName;

        public bool IsLoggedIn() => UserSettings.IsLoggedIn;

        public void SetPassword(string password) => UserSettings.Password = password;

        public void SetToken(string token) => UserSettings.Token = token;

        public void SetTokenExpireDate(string tokenExpireDate) => UserSettings.TokenExpireDate = tokenExpireDate;

        public void SetUser(User user)
        {
            UserSettings.UserName = user.Username;
            UserSettings.UserCode = user.Code;
            UserSettings.Token = user.Token;
            UserSettings.TokenExpireDate = user.Token_Expire_Date.ToString();
            UserSettings.IsLoggedIn = true;
            UserSettings.Password = string.Empty;
        }

        public void SetUserCode(string code) => UserSettings.UserCode = code;

        public void SetUserName(string userName) => UserSettings.UserName = userName;
    }
}
