﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System.Diagnostics.CodeAnalysis;

namespace ExampleApp.Shared.Services.Wrappers.Credential
{
    [ExcludeFromCodeCoverage]
    public class UserSettings
    {
        static ISettings AppSettings
        {
            get => CrossSettings.Current;
        }

        public static string Password
        {
            get => AppSettings.GetValueOrDefault(nameof(Password), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(Password), value);
        }

        public static string UserCode
        {
            get => AppSettings.GetValueOrDefault(nameof(UserCode), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(UserCode), value);
        }

        public static string UserName
        {
            get => AppSettings.GetValueOrDefault(nameof(UserName), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(UserName), value);
        }

        public static string Token
        {
            get => AppSettings.GetValueOrDefault(nameof(Token), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(Token), value);
        }

        public static string TokenExpireDate
        {
            get => AppSettings.GetValueOrDefault(nameof(TokenExpireDate), null);
            set => AppSettings.AddOrUpdateValue(nameof(TokenExpireDate), value);
        }

        public static bool IsLoggedIn
        {
            get => AppSettings.GetValueOrDefault(nameof(IsLoggedIn), false);
            set => AppSettings.AddOrUpdateValue(nameof(IsLoggedIn), value);
        }

        public static void ClearAllData()
        {
            AppSettings.Clear();
        }
    }
}
