﻿using ExampleApp.Shared.Models;

namespace ExampleApp.Shared.Services.Wrappers.Credential
{
    public interface IUserSettings
    {
        void SetUser(User user);
        User GetUser();
        string GetUserName();
        void SetUserName(string userName);
        string GetPassword();
        void SetPassword(string password);
        string GetUserCode();
        void SetUserCode(string code);
        string GetToken();
        void SetToken(string token);
        string GetTokenExpireDate();
        void SetTokenExpireDate(string tokenExpireDate);
        bool IsLoggedIn();
        void ClearAllData();
    }
}
