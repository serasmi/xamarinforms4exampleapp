﻿using Xamarin.Essentials;
using Xamarin.Forms;

namespace ExampleApp.Shared.Services.Wrappers.AppStaticClass
{
    public class ApplicationWrapper : IApp
    {
        public void ChangeMainPage(Page page)
        {
            Shell.Current.Navigation.PopModalAsync(true);
        }

        public Page GetMainPage()
        {
            return Application.Current.MainPage;
        }

        public bool IsPlatformSupported()
        {
            return DeviceInfo.Platform != DevicePlatform.Unknown;
        }
    }
}
