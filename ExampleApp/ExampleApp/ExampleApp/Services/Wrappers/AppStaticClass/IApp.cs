﻿using Xamarin.Forms;

namespace ExampleApp.Shared.Services.Wrappers.AppStaticClass
{
    public interface IApp
    {
        void ChangeMainPage(Page page);
        Page GetMainPage();
        bool IsPlatformSupported();
    }
}
