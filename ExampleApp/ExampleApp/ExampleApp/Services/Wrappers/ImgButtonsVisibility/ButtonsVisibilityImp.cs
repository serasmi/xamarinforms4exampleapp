﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ExampleApp.Shared.Services.Wrappers.ImgButtonsVisibility
{
    public class ButtonsVisibilityImp : IButtonsVisibility
    {
        AbsoluteLayout MyLayout;
        Dictionary<string, Button> Frames;

        public ButtonsVisibilityImp(AbsoluteLayout _myLayout, Dictionary<string, Button> frames)
        {
            MyLayout = _myLayout;
            Frames = frames;
        }

        public void SetButtonsVisibility(List<object> choosenItems, ref bool EditVisibility, ref bool DeleteVisibility)
        {
            SetEditButtonVisibility(choosenItems, ref EditVisibility);
            SetRemoveButtonVisibility(choosenItems, ref DeleteVisibility);
        }

        public void SetEditButtonVisibility(List<object> choosenItems, ref bool EditVisibility)
        {
            if (choosenItems != null && EditVisibility != (choosenItems.Count == 1))
            {
                EditVisibility = choosenItems.Count == 1;

                if (EditVisibility)
                {
                    MyLayout.Children.Add(Frames["edit"]);
                    Frames["edit"].FadeTo(1, 400, Easing.SpringIn);
                }
                else
                {
                    Frames["edit"].FadeTo(0, 400, Easing.SpringOut);
                    MyLayout.Children.Remove(Frames["edit"]);
                }
            }
        }

        public void SetRemoveButtonVisibility(List<object> choosenItems, ref bool DeleteVisibility)
        {
            if (choosenItems != null && DeleteVisibility != choosenItems.Count > 0)
            {
                DeleteVisibility = choosenItems.Count > 0;

                if (DeleteVisibility)
                {
                    MyLayout.Children.Add(Frames["remove"]);
                    Frames["remove"].FadeTo(1, 400, Easing.SpringIn);
                }
                else
                {
                    Frames["remove"].FadeTo(0, 400, Easing.SpringOut);
                    MyLayout.Children.Remove(Frames["remove"]);
                }
            }
        }
    }
}
