﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExampleApp.Shared.Services.Wrappers.ImgButtonsVisibility
{
    public interface IButtonsVisibility
    {
        void SetButtonsVisibility(List<object> choosenItems, ref bool EditVisibility, ref bool DeleteVisibility);
        void SetEditButtonVisibility(List<object> choosenItems, ref bool EditVisibility);
        void SetRemoveButtonVisibility(List<object> choosenItems, ref bool DeleteVisibility);
    }
}
