﻿using ExampleApp.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExampleApp.Shared.Services
{
    public interface IMyItemsService
    {
        Task<bool> AddItem(Item myItem);
        Task RemoveItem(string id);
        Task UpdateItem(Item updatedItem);
        Task<List<Item>> GetItems(uint page, uint itemsPerPage, string name);
    }
}
