﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows.Input;
using Xamarin.Forms;

namespace ExampleApp.Shared.Behaviors
{
    [ExcludeFromCodeCoverage]
    public class SearchValidator : Behavior<Entry>
    {
        public static readonly BindableProperty CommandProperty =
            BindableProperty.Create(
                propertyName: nameof(Command),
                returnType: typeof(ICommand),
                declaringType: typeof(SearchValidator));

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        protected override void OnAttachedTo(Entry bindable)
        {
            base.OnAttachedTo(bindable);
            bindable.TextChanged += BindableOnTextChanged;
            bindable.BindingContextChanged += BindableOnBindingContextChanged;
        }

        private void BindableOnBindingContextChanged(object sender, EventArgs e)
        {
            var entry = sender as Entry;
            BindingContext = entry?.BindingContext;
        }

        private void BindableOnTextChanged(object sender, TextChangedEventArgs e)
        {
            var entry = sender as Entry;
            //if (entry.Text.Length > 5 || entry.Text.Length == 0)
            Command.Execute(null);
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            base.OnDetachingFrom(bindable);
            bindable.TextChanged -= BindableOnTextChanged;
            bindable.BindingContextChanged -= BindableOnBindingContextChanged;
        }
    }
}
