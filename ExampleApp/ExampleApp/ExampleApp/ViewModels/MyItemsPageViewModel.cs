﻿using ExampleApp.Shared.Models;
using ExampleApp.Shared.Services;
using ExampleApp.Shared.Services.Wrappers.Connectivity;
using ExampleApp.Shared.Services.Wrappers.Credential;
using ExampleApp.Shared.Services.Wrappers.ImgButtonsVisibility;
using ExampleApp.Shared.ViewModels.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ExampleApp.Shared.ViewModels
{
    public class MyItemsPageViewModel : InheritedViewModels.LoadMoreItemsViewModel
    {
        public readonly IMyItemsService myItems;
        public readonly IUserSettings userSettings;
        public readonly IButtonsVisibility buttonsVisibility;

        public ObservableCollection<Item> Items { get; set; } = new ObservableCollection<Item>();
        public List<Object> ChoosenItems { get; set; } = new List<Object>();

        public ICommand TextChanged { get; set; }
        public ICommand SelectionChanged { get; set; }

        public ICommand LoadCommand { get; set; }

        public ICommand EditCommand { get; set; }
        public ICommand RemoveCommand { get; set; }

        public bool DeleteVisibility = false;
        public bool EditVisibility = false;

        public MyItemsPageViewModel(IMyItemsService _itemsService, IButtonsVisibility _btnVisibility, IConnectivity _connectivity, IUserSettings _userSettings) : base(_itemsService, _connectivity)
        {
            myItems = _itemsService;
            buttonsVisibility = _btnVisibility;
            userSettings = _userSettings;

            buttonsVisibility.SetButtonsVisibility(ChoosenItems, ref EditVisibility, ref DeleteVisibility);

            LoadCommand = new RelayCommand(async (object arg) => await ExecuteLoadCommand(arg));
            TextChanged = new RelayCommand((object arg) => OnTextChanged());
            RemoveCommand = new RelayCommand((object arg) => RemoveItem(arg));
            SelectionChanged = new RelayCommand((object arg) => buttonsVisibility.SetButtonsVisibility(ChoosenItems, ref EditVisibility, ref DeleteVisibility));
        }

        public async Task ExecuteLoadCommand(object obj)
        {
            bool refresh = (bool)obj;

            if (IsBusy || IsFinalPage)
            {
                IsBusy = false;
                return;
            }

            try
            {
                var items = new List<Item>();

                if (refresh && Items.Count > 0)
                {
                    Items.Clear();
                    OnCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedAction.Reset);
                    items = await GetItemsList(true);
                }
                else
                {
                    items = await GetItemsList();
                }
                
                foreach (Item item in items)
                {
                    Items.Add(item);
                    OnCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedAction.Add, "Items");
                }

                IsFinalPage = Items.Count % ItemsPerPage != 0;
                Page = IsFinalPage ? Page : ++Page;
            }
            catch (NullReferenceException e)
            {
                throw e;
            }
            catch (AggregateException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void OnTextChanged()
        {
            this.OnPropertyChanged("SearchValue");
            ChoosenItems.Clear();
            LoadCommand.Execute(true);
        }

        public void RemoveItem(object itemObject)
        {
            var item = (Item)itemObject;

            try
            {
                if (item != null)
                {
                    myItems.RemoveItem(item.Id);
                    Items.Remove(item);
                }
            }
            catch(Exception e)
            {
                throw e;
            }
        }
    }
}
