﻿using ExampleApp.Shared.Models;
using ExampleApp.Shared.Services;
using ExampleApp.Shared.Services.Wrappers.AppStaticClass;
using ExampleApp.Shared.Services.Wrappers.Connectivity;
using ExampleApp.Shared.Services.Wrappers.Credential;
using ExampleApp.Shared.ViewModels.Commands;
using System;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace ExampleApp.Shared.ViewModels
{
    public class LoginPageViewModel : InheritedViewModels.BaseViewModel
    {
        public readonly IAuth authService;
        public readonly IUserSettings userSettings;
        public readonly IConnectivity connectivity;
        public readonly IApp application;

        public User User
        {
            get => userSettings.GetUser();
            set => userSettings.SetUser(value);
        }

        public string UserName
        {
            get => userSettings.GetUserName();
            set
            {
                userSettings.SetUserName(value);
                OnPropertyChanged("UserName");
            }
        }

        public string Password
        {
            get => userSettings.GetPassword();
            set
            {
                userSettings.SetPassword(value);
                OnPropertyChanged("Password");
            }
        }

        private bool canExecute;
        public bool CanExecute
        {
            get => canExecute;
            set
            {
                canExecute = value;
                LoginCommand.RaiseCanExecuteChanged();
            }
        }

        private RelayCommand loginCommand;
        public RelayCommand LoginCommand
        {
            get => loginCommand;
            set => loginCommand = value;
        }

        private Page page;
        public Page Page
        {
            get => page = page ??
                new AppShell();
            set => page = value;
        }

        public LoginPageViewModel(IAuth _authService, IUserSettings _userSettings, IConnectivity _connectivity, IApp _app) : base(_connectivity)
        {
            authService = _authService;
            userSettings = _userSettings;
            connectivity = _connectivity;
            application = _app;

            LoginCommand = new RelayCommand(async (object arg) => await Authenticate(arg), (object arg) => { return CanExecute; });
        }

        public async Task Authenticate(object param)
        {
            var current = connectivity.GetNetworkState();

            if (current == NetworkAccess.Internet)
            {
                User user = (param as User);

                var userData = await this.authService.Authenticate(user);

                if (String.IsNullOrEmpty(userData?.Username))
                {
                    MessagingCenter.Send<LoginPageViewModel, string>(this, "LoginError", "Podałeś zły login lub hasło.");
                    this.Password = string.Empty;
                }
                else
                {
                    User = userData;
                    if (this.application.IsPlatformSupported())
                    {
                        this.application.ChangeMainPage(page);
                    }
                }
            }
            else
            {
                MessagingCenter.Send<LoginPageViewModel, string>(this, "InternetChanged", "Sprawdź połączenie internetowe!");
            }
        }

        public void CheckIfCanExecute()
        {
            if (String.IsNullOrEmpty(Password) || String.IsNullOrEmpty(UserName))
            {
                CanExecute = false;
            }
            else
            {
                CanExecute = true;
            }
        }
    }
}
