﻿using System;
using System.Windows.Input;

namespace ExampleApp.Shared.ViewModels.Commands
{
    public class RelayCommand : ICommand
    {
        private Action<object> execute;
        private readonly Func<object, bool> canExecute;

        public event EventHandler CanExecuteChanged;

        public RelayCommand(
            Action<object> execute)
        {
            this.execute = execute;
        }

        public RelayCommand(
            Action<object> execute,
            Func<object, bool> canExecute)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecute == null || this.canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            this.execute(parameter);
        }

        public virtual void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, new EventArgs());
        }
    }
}
