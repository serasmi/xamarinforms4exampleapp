﻿using ExampleApp.Shared.Models;
using ExampleApp.Shared.Services;
using ExampleApp.Shared.Services.Wrappers.Connectivity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExampleApp.Shared.ViewModels.InheritedViewModels
{
    public class LoadMoreItemsViewModel : BaseViewModel
    {
        private IMyItemsService itemsService;

        private uint itemsPerPage = 16;
        public uint ItemsPerPage
        {
            get => itemsPerPage;
            set => itemsPerPage = value;
        }

        private uint page = 0;
        public uint Page
        {
            get => page;
            set => page = value;
        }

        private string searchValue;
        public string SearchValue
        {
            get => searchValue;
            set
            {
                searchValue = value;
                OnPropertyChanged();
            }
        }

        private bool isFinalPage = false;
        public bool IsFinalPage
        {
            get => isFinalPage;
            set => isFinalPage = value;
        }

        public LoadMoreItemsViewModel(IMyItemsService _items, IConnectivity _conn) : base(_conn)
        {
            itemsService = _items;
        }

        public async Task<List<Item>> GetItemsList(bool refresh = false)
        {
            IsBusy = true;

            var items = new List<Item>();

            try
            {
                if (refresh)
                {
                    Page = 0;
                }
                items = await itemsService.GetItems(Page, itemsPerPage, SearchValue);
            }
            catch(Exception e)
            {
                throw e;
            }

            IsBusy = false;
            Page++;
            return items;
        }
    }
}
