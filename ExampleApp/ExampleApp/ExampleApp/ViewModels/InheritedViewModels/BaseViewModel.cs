﻿using ExampleApp.Shared.Services.Wrappers.Connectivity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace ExampleApp.Shared.ViewModels.InheritedViewModels
{
    public class BaseViewModel : INotifyPropertyChanged, INotifyCollectionChanged
    {
        private IConnectivity connectivity;

        bool isBusy = false;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        private bool isInternetEnabled;
        public bool IsInternetEnabled
        {
            get => isInternetEnabled;
            set
            {
                isInternetEnabled = value;
                OnPropertyChanged();
            }
        }

        public BaseViewModel(IConnectivity _connectivity)
        {
            connectivity = _connectivity;
            connectivity.SetEventHandlerToConnectivityChanged(OnConnectivityChanged);
        }

        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName] string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        public void OnConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            if (!CheckConnectivity())
            {
                IsInternetEnabled = false;
                MessagingCenter.Send<BaseViewModel, string>(this, "InternetChanged", "Sprawdź połączenie internetowe!");
            }
            else
            {
                IsInternetEnabled = true;
            }
        }

        public bool CheckConnectivity()
        {
            var current = this.connectivity.GetNetworkState();

            return current == NetworkAccess.Internet;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if(changed == null)
            {
                return;
            }

            changed?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region INotifyCollectionChanged
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        protected virtual void OnCollectionChanged(NotifyCollectionChangedAction action, [CallerMemberName] string propertyName = "")
        {
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(action, propertyName));
        }
        #endregion
    }
}
