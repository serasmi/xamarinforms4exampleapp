﻿using ExampleApp.Shared.Models;
using ExampleApp.Shared.Services;
using ExampleApp.Shared.Services.Wrappers.Connectivity;
using ExampleApp.Shared.ViewModels.InheritedViewModels;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ExampleApp.Tests
{
    public class LoadMoreItemsViewModelTests
    {
        [Fact]
        public async Task Get_Returns_Items()
        {
            Mock<IMyItemsService> itemsMock = new Mock<IMyItemsService>();
            itemsMock.Setup(s => s.GetItems(It.IsAny<uint>(), It.IsAny<uint>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new List<Item>
                {
                    new Item{ Id = "test" },
                    new Item{ Id = Guid.NewGuid().ToString() },
                    new Item{ Id = Guid.NewGuid().ToString() }
                }));

            Mock<IConnectivity> connMock = new Mock<IConnectivity>();

            //Arrange - target
            LoadMoreItemsViewModel target = new LoadMoreItemsViewModel(itemsMock.Object, connMock.Object);

            // Act
            var list = await target.GetItemsList();

            // Assert
            Assert.True(list.Count == 3);
            Assert.Equal("test", list[0].Id);
        }

        [Fact]
        public async Task Get_Contractors_Returns_Items_By_Search_Value()
        {
            Mock<IMyItemsService> itemsMock = new Mock<IMyItemsService>();
            itemsMock.Setup(s => s.GetItems(It.IsAny<uint>(), It.IsAny<uint>(), It.IsAny<string>()))
                .Returns(Task.FromResult(new List<Item>
                {
                    new Item{ Id = Guid.NewGuid().ToString(), Name = "test" },
                    new Item{ Id = Guid.NewGuid().ToString() },
                    new Item{ Id = Guid.NewGuid().ToString() }
                }));

            Mock<IConnectivity> connMock = new Mock<IConnectivity>();

            //Arrange - target
            LoadMoreItemsViewModel target = new LoadMoreItemsViewModel(itemsMock.Object, connMock.Object);

            // Act
            target.SearchValue = "test";
            var list = await target.GetItemsList(true);

            // Assert
            Assert.True(list.Count == 3);
            Assert.Equal("test", list[0].Name);
        }

        [Fact]
        public async void Get_Next_Page_Service_Throws_An_Exception()
        {
            Mock<IMyItemsService> itemsMock = new Mock<IMyItemsService>();
            itemsMock.Setup(s => s.GetItems(It.IsAny<uint>(), It.IsAny<uint>(), It.IsAny<string>()))
                .Throws(new System.Exception());

            Mock<IConnectivity> connMock = new Mock<IConnectivity>();

            //Arrange - target
            LoadMoreItemsViewModel target = new LoadMoreItemsViewModel(itemsMock.Object, connMock.Object);

            // Act
            target.Page = 0;
            target.SearchValue = "";

            // Assert
            await Assert.ThrowsAsync<Exception>(() => target.GetItemsList());
        }
    }
}
