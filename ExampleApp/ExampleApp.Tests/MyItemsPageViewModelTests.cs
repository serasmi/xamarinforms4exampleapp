﻿using Xunit;
using Moq;
using ExampleApp.Shared.Services;
using ExampleApp.Shared.Services.Wrappers.ImgButtonsVisibility;
using ExampleApp.Shared.Services.Wrappers.Connectivity;
using ExampleApp.Shared.Services.Wrappers.Credential;
using ExampleApp.Shared.ViewModels;
using System.Threading.Tasks;
using System;
using ExampleApp.Shared.Models;

namespace ExampleApp.Tests
{
    public class MyItemsPageViewModelTests
    {
        [Fact]
        public void Constructor_Apply_Parameters_To_Local_Variables()
        {
            // Arrange
            Mock<IMyItemsService> itemsMock = new Mock<IMyItemsService>();
            Mock<IButtonsVisibility> btnVisibilityMock = new Mock<IButtonsVisibility>();
            Mock<IConnectivity> connMock = new Mock<IConnectivity>();
            Mock<IUserSettings> userSettingsMock = new Mock<IUserSettings>();

            var target = new MyItemsPageViewModel(itemsMock.Object, btnVisibilityMock.Object, connMock.Object, userSettingsMock.Object);

            // Assert
            Assert.Equal(itemsMock.Object, target.myItems);
            Assert.Equal(btnVisibilityMock.Object, target.buttonsVisibility);
            Assert.Equal(userSettingsMock.Object, target.userSettings);

            Assert.NotNull(target.LoadCommand);
        }

        [Fact]
        public async Task ExecuteLoadCommand_Completes_Items_Variable()
        {
            // Arrange
            Mock<IMyItemsService> itemsMock = new Mock<IMyItemsService>();
            itemsMock.Setup(s => s.GetItems(It.IsAny<uint>(), It.IsAny<uint>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new System.Collections.Generic.List<Shared.Models.Item>
                    {
                        new Shared.Models.Item(),
                        new Shared.Models.Item(),
                        new Shared.Models.Item()
                    }));

            Mock<IButtonsVisibility> btnVisibilityMock = new Mock<IButtonsVisibility>();
            Mock<IConnectivity> connMock = new Mock<IConnectivity>();
            Mock<IUserSettings> userSettingsMock = new Mock<IUserSettings>();

            var target = new MyItemsPageViewModel(itemsMock.Object, btnVisibilityMock.Object, connMock.Object, userSettingsMock.Object);

            // Act
            await target.ExecuteLoadCommand(true);

            // Assert
            Assert.True(target.Items.Count == 3);
        }

        [Fact]
        public async Task ExecuteLoadCommand_Supply_Items_Variable()
        {
            // Arrange
            Mock<IMyItemsService> itemsMock = new Mock<IMyItemsService>();
            itemsMock.Setup(s => s.GetItems(It.IsAny<uint>(), It.IsAny<uint>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new System.Collections.Generic.List<Shared.Models.Item>
                    {
                        new Shared.Models.Item(),
                        new Shared.Models.Item(),
                        new Shared.Models.Item()
                    }));

            Mock<IButtonsVisibility> btnVisibilityMock = new Mock<IButtonsVisibility>();
            Mock<IConnectivity> connMock = new Mock<IConnectivity>();
            Mock<IUserSettings> userSettingsMock = new Mock<IUserSettings>();

            var target = new MyItemsPageViewModel(itemsMock.Object, btnVisibilityMock.Object, connMock.Object, userSettingsMock.Object);
            target.Items = new System.Collections.ObjectModel.ObservableCollection<Shared.Models.Item>
            {
                new Shared.Models.Item(),
                new Shared.Models.Item(),
                new Shared.Models.Item()
            };

            // Act
            await target.ExecuteLoadCommand(false);

            // Assert
            Assert.True(target.Items.Count == 6);
        }

        [Fact]
        public async Task ExecuteLoadCommand_ItemsService_Throws_An_Exception()
        {
            // Arrange
            Mock<IMyItemsService> itemsMock = new Mock<IMyItemsService>();
            itemsMock.Setup(s => s.GetItems(It.IsAny<uint>(), It.IsAny<uint>(), It.IsAny<string>()))
                .Throws(new Exception());

            Mock<IButtonsVisibility> btnVisibilityMock = new Mock<IButtonsVisibility>();
            Mock<IConnectivity> connMock = new Mock<IConnectivity>();
            Mock<IUserSettings> userSettingsMock = new Mock<IUserSettings>();

            var target = new MyItemsPageViewModel(itemsMock.Object, btnVisibilityMock.Object, connMock.Object, userSettingsMock.Object);
            
            // Assert
            await Assert.ThrowsAsync<Exception>(async () => await target.ExecuteLoadCommand(true));
        }

        [Fact]
        public async Task OnTextChanged_Uses_Service()
        {
            // Arrange
            Mock<IMyItemsService> itemsMock = new Mock<IMyItemsService>();
            itemsMock.Setup(s => s.GetItems(It.IsAny<uint>(), It.IsAny<uint>(), It.IsAny<string>()))
                .Returns(Task.FromResult(
                    new System.Collections.Generic.List<Shared.Models.Item>
                    {
                        new Shared.Models.Item(),
                        new Shared.Models.Item(),
                        new Shared.Models.Item()
                    }))
                .Verifiable();

            Mock<IButtonsVisibility> btnVisibilityMock = new Mock<IButtonsVisibility>();
            Mock<IConnectivity> connMock = new Mock<IConnectivity>();
            Mock<IUserSettings> userSettingsMock = new Mock<IUserSettings>();

            var target = new MyItemsPageViewModel(itemsMock.Object, btnVisibilityMock.Object, connMock.Object, userSettingsMock.Object);
            target.OnTextChanged();

            itemsMock.Verify(s => s.GetItems(It.IsAny<uint>(), It.IsAny<uint>(), It.IsAny<string>()), Times.Once);
            Assert.True(target.Items.Count == 3);
        }

        [Fact]
        public void RemoveItem_Method_Object_From_List()
        {
            // Arrange
            Mock<IMyItemsService> itemsMock = new Mock<IMyItemsService>();
            Mock<IButtonsVisibility> btnVisibilityMock = new Mock<IButtonsVisibility>();
            Mock<IConnectivity> connMock = new Mock<IConnectivity>();
            Mock<IUserSettings> userSettingsMock = new Mock<IUserSettings>();

            var target = new MyItemsPageViewModel(itemsMock.Object, btnVisibilityMock.Object, connMock.Object, userSettingsMock.Object);
            target.Items = new System.Collections.ObjectModel.ObservableCollection<Shared.Models.Item>
            {
                new Item
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "This is first item",
                    Description = "First test item added to list"
                },
                new Item
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "This is second item",
                    Description = "Second test item added to list"
                }
            };

            var thirdItem = new Item
            {
                Id = Guid.NewGuid().ToString(),
                Name = "This is third item",
                Description = "Third test item added to list"
            };

            target.Items.Add(thirdItem);

            target.RemoveItem(thirdItem);

            Assert.True(target.Items.Count == 2);
            Assert.Equal("This is first item", target.Items[0].Name);
            Assert.Equal("This is second item", target.Items[1].Name);
        }

        [Fact]
        public void RemoveItem_Throws_An_Exception()
        {
            // Arrange
            Mock<IMyItemsService> itemsMock = new Mock<IMyItemsService>();
            itemsMock.Setup(s => s.RemoveItem(It.IsAny<string>()))
                .Throws(new Exception());

            Mock<IButtonsVisibility> btnVisibilityMock = new Mock<IButtonsVisibility>();
            Mock<IConnectivity> connMock = new Mock<IConnectivity>();
            Mock<IUserSettings> userSettingsMock = new Mock<IUserSettings>();

            var target = new MyItemsPageViewModel(itemsMock.Object, btnVisibilityMock.Object, connMock.Object, userSettingsMock.Object);
            target.Items = new System.Collections.ObjectModel.ObservableCollection<Shared.Models.Item>
            {
                new Item
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "This is first item",
                    Description = "First test item added to list"
                },
                new Item
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "This is second item",
                    Description = "Second test item added to list"
                }
            };

            var thirdItem = new Item
            {
                Id = Guid.NewGuid().ToString(),
                Name = "This is third item",
                Description = "Third test item added to list"
            };

            target.Items.Add(thirdItem);

            Assert.Throws<Exception>(() => target.RemoveItem(thirdItem));
        }
    }
}
