﻿using ExampleApp.Shared.Models;
using ExampleApp.Shared.Services;
using ExampleApp.Shared.Services.Wrappers.AppStaticClass;
using ExampleApp.Shared.Services.Wrappers.Connectivity;
using ExampleApp.Shared.Services.Wrappers.Credential;
using ExampleApp.Shared.ViewModels;
using ExampleApp.Shared.ViewModels.InheritedViewModels;
using Moq;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xunit;

namespace ExampleApp.Tests
{
    public class LoginPageViewModelTests
    {
        [Fact]
        public void Constructor_Fills_Local_Variable_With_Parameters()
        {
            // Arrange - create authentication service mock
            Mock<IAuth> authMock = new Mock<IAuth>();

            // Arrange - create usersettings service mock
            Mock<IUserSettings> userMock = new Mock<IUserSettings>();

            // Arrange - create mock for IConnectivity
            Mock<IConnectivity> connectivity = new Mock<IConnectivity>();

            // Arrange - create mock for IApplication
            Mock<IApp> application = new Mock<IApp>();

            // Act - create view model object
            LoginPageViewModel viewModel = new LoginPageViewModel(authMock.Object, userMock.Object, connectivity.Object, application.Object);

            // Assert - constructor aplied parameters to variables
            Assert.Equal(userMock.Object, viewModel.userSettings);
            Assert.Equal(authMock.Object, viewModel.authService);
            Assert.Equal(connectivity.Object, viewModel.connectivity);
            Assert.Equal(application.Object, viewModel.application);
            Assert.NotNull(viewModel.LoginCommand);
        }

        [Fact]
        public async void Authenticate_Complements_User_Object()
        {
            // Arrange - create authentication service mock
            Mock<IAuth> authMock = new Mock<IAuth>();

            // Arrange - create usersettings service mock
            Mock<IUserSettings> userMock = new Mock<IUserSettings>();

            // Arrange - create mock for IConnectivity
            Mock<IConnectivity> connectivity = new Mock<IConnectivity>();

            // Arrange - create mock for IApplication
            Mock<IApp> application = new Mock<IApp>();
            application.Setup(s => s.IsPlatformSupported())
                .Returns(false);

            Page page = new Page();
            application.Setup(s => s.ChangeMainPage(page));

            // Arrange - user model
            var user = new User { Username = "T" };

            // Arrange - setup auth mock
            authMock.Setup(s => s.Authenticate(user))
                .Returns(Task.FromResult(new User { Username = "t" }));
            // Arrange - setup user mock
            userMock.Setup(s => s.GetUser())
                .Returns(user);

            // Assert - create view model object
            LoginPageViewModel lvm = new LoginPageViewModel(authMock.Object, userMock.Object, connectivity.Object, application.Object);
            lvm.Page = page;

            // Act
            await lvm.Authenticate(lvm);

            // Assert
            Assert.IsType<User>(lvm.User);
        }

        [Fact]
        public async void Authenticate_Service_Returns_User()
        {
            // Arrange - create authentication service mock
            Mock<IAuth> authMock = new Mock<IAuth>();

            // Arrange - create usersettings service mock
            Mock<IUserSettings> userMock = new Mock<IUserSettings>();

            // Arrange - create mock for IApplication
            Mock<IApp> application = new Mock<IApp>();
            application.Setup(s => s.IsPlatformSupported())
                .Returns(false);

            Page page = new Page();
            application.Setup(s => s.ChangeMainPage(page));

            // Arrange - user model
            var user = new User { Username = "T" };

            // Arrange - setup auth mock
            authMock.Setup(s => s.Authenticate(user))
                .Returns(Task.FromResult(user));

            // Arrange - setup user mock
            userMock.Setup(s => s.GetUser())
                .Returns(user);
            userMock.Setup(a => a.GetUserName())
                .Returns("T");

            // Arrange - create connectivity mock
            Mock<IConnectivity> connectivity = new Mock<IConnectivity>();

            // Assert - create view model object
            LoginPageViewModel lvm = new LoginPageViewModel(authMock.Object, userMock.Object, connectivity.Object, application.Object);
            lvm.Page = page;

            // Act
            await lvm.Authenticate(lvm);

            // Assert
            Assert.IsType<User>(lvm.User);
            Assert.Equal("T", lvm.User.Username);
        }

        [Theory]
        [InlineData("t", "t", true)]
        [InlineData("t", "", false)]
        [InlineData("", "t", false)]
        public void Can_Execute_Change_Object(string name, string pass, bool result)
        {
            // Arrange - create user
            User user = new User
            {
                Username = name,
                Password = pass
            };

            // Arrange - create authentication service mock
            Mock<IAuth> authMock = new Mock<IAuth>();

            // Arrange - create usersettings service mock
            Mock<IUserSettings> userMock = new Mock<IUserSettings>();

            // Arrange - create mock for IApplication
            Mock<IApp> application = new Mock<IApp>();

            // Arrange - setup userMock
            userMock.Setup(s => s.GetUserName())
                .Returns(name);
            userMock.Setup(s => s.GetPassword())
                .Returns(pass);

            // Arrange - create connectivity mock
            Mock<IConnectivity> connectivity = new Mock<IConnectivity>();

            // Assert - create view model object
            LoginPageViewModel lvm = new LoginPageViewModel(authMock.Object, userMock.Object, connectivity.Object, application.Object);

            // Assert - fill password and username string objects
            lvm.Password = user.Password;
            lvm.UserName = user.Username;

            // Act
            lvm.CheckIfCanExecute();

            // Assert
            Assert.Equal(result, lvm.CanExecute);
        }

        [Fact]
        public void Get_Page()
        {
            // Arrange - create authentication service mock
            Mock<IAuth> authMock = new Mock<IAuth>();

            // Arrange - create usersettings service mock
            Mock<IUserSettings> userMock = new Mock<IUserSettings>();

            // Arrange - create mock for IApp
            Mock<IApp> application = new Mock<IApp>();

            // Arrange - create connectivity mock
            Mock<IConnectivity> connectivity = new Mock<IConnectivity>();

            // Arrange - create view model object
            LoginPageViewModel vm = new LoginPageViewModel(authMock.Object, userMock.Object, connectivity.Object, application.Object);

            // Arrange - create page object
            Page page = new Page();

            // Act - fill page
            vm.Page = page;

            // Assert
            Assert.Equal(page, vm.Page);
        }

        [Fact]
        public async void Check_If_Password_Is_Set_To_Empty()
        {
            // Arrange - create user object
            User user = new User();

            // Arrange - create authentication service mock
            Mock<IAuth> authMock = new Mock<IAuth>();
            authMock.Setup(s => s.Authenticate(user))
                .Returns(Task.FromResult(new User()));

            // Arrange - create usersettings service mock
            Mock<IUserSettings> userMock = new Mock<IUserSettings>();

            // Arrange - create mock for IApp
            Mock<IApp> application = new Mock<IApp>();

            // Arrange - create connectivity mock
            Mock<IConnectivity> connectivity = new Mock<IConnectivity>();

            // Arrange - create view model object
            LoginPageViewModel vm = new LoginPageViewModel(authMock.Object, userMock.Object, connectivity.Object, application.Object);

            // Act
            await vm.Authenticate(vm);

            // Assert
            Assert.True(vm.Password == string.Empty || vm.Password == null);
        }

        [Fact]
        public void Application_Is_Sending_Message_When_Internet_Connection_Changed()
        {
            // Arrange - create user object
            User user = new User();

            // Arrange - create authentication service mock
            Mock<IAuth> authMock = new Mock<IAuth>();
            authMock.Setup(s => s.Authenticate(user))
                .Returns(Task.FromResult(new User()));

            // Arrange - create usersettings service mock
            Mock<IUserSettings> userMock = new Mock<IUserSettings>();

            // Arrange - create mock for IApp
            Mock<IApp> application = new Mock<IApp>();

            // Arrange - create connectivity mock
            Mock<IConnectivity> connectivity = new Mock<IConnectivity>();
            connectivity.Setup(s => s.GetNetworkState())
                .Returns(Xamarin.Essentials.NetworkAccess.None);

            // Arrange - create view model object
            LoginPageViewModel vm = new LoginPageViewModel(authMock.Object, userMock.Object, connectivity.Object, application.Object);

            // Arrange - message from messagingcenter
            string message = string.Empty;

            // Arrange - set messaging center
            MessagingCenter.Subscribe<BaseViewModel, string>(this, "InternetChanged", (sender, arg) =>
            {
                message = arg;
            });

            // Arrange - create an enumerable
            var enumerable = Enumerable.Empty<Xamarin.Essentials.ConnectionProfile>();

            // Act
            vm.OnConnectivityChanged(new object(), new Xamarin.Essentials.ConnectivityChangedEventArgs(new Xamarin.Essentials.NetworkAccess(), enumerable));

            // Assert
            Assert.Equal("Sprawdź połączenie internetowe!", message);
        }

        [Fact]
        public async void Application_Is_Changing_Page()
        {
            // Arrange - create authentication service mock
            Mock<IAuth> authMock = new Mock<IAuth>();
            authMock.Setup(s => s.Authenticate(It.IsAny<User>()))
                .Returns(Task.FromResult(new User
                {
                    Username = "test"
                }));

            // Arrange - create usersettings service mock
            Mock<IUserSettings> userMock = new Mock<IUserSettings>();
            userMock.Setup(s => s.GetUser())
                .Returns(new User
                {
                    Username = "test"
                });

            // Arrange - set page
            Page page = new Page();

            // Arrange - create mock for IApp
            Mock<IApp> application = new Mock<IApp>();
            application.Setup(s => s.IsPlatformSupported())
                .Returns(true);
            application.Setup(s => s.ChangeMainPage(It.IsAny<Page>())).Verifiable();


            // Arrange - create connectivity mock
            Mock<IConnectivity> connectivity = new Mock<IConnectivity>();
            connectivity.Setup(s => s.GetNetworkState())
                .Returns(Xamarin.Essentials.NetworkAccess.Internet);

            // Arrange - create view model object
            LoginPageViewModel vm = new LoginPageViewModel(authMock.Object, userMock.Object, connectivity.Object, application.Object);
            vm.Page = page;

            // Act
            await vm.Authenticate(new User());

            // Assert
            application.Verify(s => s.ChangeMainPage(page), Times.Once);
        }
    }
}
